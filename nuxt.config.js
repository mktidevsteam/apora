// const pkg = require('./package')

module.exports = {
  mode: "universal",

  css: [
    { src: '~assets/materialize_mkti.scss', lang: 'scss' }
  ],

  head: {
    title: 'Apora • Clínica de Antienvejecimiento y Age Management en Guadalajara',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Los tratamientos más exlucsivos y avanzados a nivel mundial, ahora en Guadalajara. Contamos con un área específica para contrarrestar los efectos que el paso del tiempo va causando en nuestro organismo.' }
    ],
    script: [
      { src: "/js/materialize.js" }
      // { src: "/js/Backend.js" },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: {
    color: '#3B8070',
    height: '2px'
  },

  modules: [
    '@nuxtjs/axios',
    ['@nuxtjs/google-tag-manager', { id: 'GTM-MJ9NKD9', pageTracking: true }],
  ],

  plugins: [
    { src: '~/plugins/gtag.js', ssr: false },
    // { src: '~/plugins/MiniBackend.js', ssr: false },
    { src: '~/plugins/eventBus.js', ssr: true },
    { src: '~/plugins/filters.js', ssr: false },
  ],

  env: {
    'backend_dir_dev': "http://localhost/cluster3/apora_nuxt/static/api",
    'backend_dir_gen': "https://api.clinicaapora.mx"
  },

  build: {
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          options: { emitWarning: true },
          exclude: /(node_modules)/
        })
      }
    }
  }

}
