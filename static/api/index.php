<?php
include_once("_system_includes.php");

//** ROUTES **//
$Router->REQUEST("/submit_form", function($view=null, $name=null, $email=null, $phone=null, $message=null, $date=null, $time=null) use ($SQL, $Emailer){
  $salesTeam = ["recepcion@clinicaapora.mx","luis.g.luna18@gmail.com"];

  $r = jheader();
  $r["data"] = $_REQUEST;

  if( $name=='' ){
    $r["message"] = "* Falta tu nombre.";
    recho($r);}
  if( $email=='' ){
    $r["message"] = "* Falta tu email.";
    recho($r);}
  if( $phone=='' ){
    $r["message"] = "* Falta tu teléfono.";
    recho($r);}

  if( $view == 'cita' ){
    if( $date == '' ){
      $r["message"] = "* Falta la fecha para tu cita.";
      recho($r);}
    if( $time == '' ){
      $r["message"] = "* Falta el horario de tu cita.";
      recho($r);}
  }else{
    $date = "";
    $time = "";
  }

  // Enviar al Equipo de Ventas.
  $html_template = Templater::Load("./sales_email.html");
  $html_message = Templater::ApplyAndClear($_REQUEST, $html_template);
  foreach( $salesTeam as $sales_email ){
    @$Emailer->Send($sales_email, "Nueva formulario de '$name'", $html_message);
  }

  // Guardar en SQL.
  $SQL->Create("leads", [
    'lead_type' => $view,
    'lead_name' => $name,
    'lead_email' => $email,
    'lead_phone' => $phone,
    'lead_date' => $date,
    'lead_time' => $time,
    'lead_message' => $message
  ]);

  // Guardar en Log.
  // $file = fopen('leads.txt', 'a');
  // $_REQUEST["date"] = date("Y-m-d H:i:s");
  // fwrite($file, implode("\t", array_values($_REQUEST) ). "\n");
  // fclose($file);

  $r["status"] = true;
  recho( $r );

});
