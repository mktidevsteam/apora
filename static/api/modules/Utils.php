<?php
  function Unix2Datetime($timestamp){
    return gmdate("Y-m-d\TH:i:s\Z", $timestamp);
  }
  function CurrentDatetime(){
    return gmdate("Y-m-d\TH:i:s\Z");
  }
  function DatePlus($hours=1){
    return gmdate("Y-m-d\TH:i:s\Z", strtotime("+{$hours} hours") );
  }

  function file_friendly_date(){
    return gmdate("Y_m_d");
  }

  function has_value($key, $Assoc){
    if( has_key($key, $Assoc) ){
      $value = $Assoc[$key];
      if( is_null($value) || $value == "" ){
        return false;
      }else{
        return true;
      }
    }else{
      return false;
    }
  }
  function has_key($key, $Array){
    return key_in($key, $Array);
  }
  function key_in($key, $Array){
    if( is_assoc($Array) ){
      return array_key_exists($key, $Array);
    }else{
      return in_array($key, $Array);
    }

  }
  function is_assoc($arr){
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
  }
  function is_table($Data){
    if( !is_assoc($Data) ){
      if( is_array($Data) ){
        return is_assoc( $Data[0] );
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  function RandomToken($length=200, $characters='-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'){
    return randomString($length, $characters);
  }
  function randomString($length=60, $characters='abcdefghijklmnopqrstuvwxyz0123456789'){
    $random_string = '';
    $max = strlen($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
      $random_string .= $characters[mt_rand(0, $max)];
    }
    return $random_string;
  }
  function getIP() {
   $ipaddress = '';
   if (isset($_SERVER['HTTP_CLIENT_IP']))
    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
   else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
   else if(isset($_SERVER['HTTP_X_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
   else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
   else if(isset($_SERVER['HTTP_FORWARDED']))
    $ipaddress = $_SERVER['HTTP_FORWARDED'];
   else if(isset($_SERVER['REMOTE_ADDR']))
    $ipaddress = $_SERVER['REMOTE_ADDR'];
   else
    $ipaddress = 'UNKNOWN';
   return $ipaddress;
  }

  function set_cookie($name, $value, $days=180){
    $_COOKIE[$name] = $value;
    setcookie($name, $value, time() + (86400 * 30) * $days, "/");
  }
  function get_cookie($name){
    if( isset_cookie($name) ){
      return $_COOKIE[$name];
    }else{
      return NULL;
    }
  }
  function unset_cookie($name){
    setcookie( $name, "", time() - 3600);
    unset( $_COOKIE[$name] );
  }
  function isset_cookie($name){
    return isset($_COOKIE[$name]);
  }

  function Table2List($Table){
    $List = [];
    foreach($Table as $Row){
      foreach($Row as $key => $val){
        $List[] = $val;
      }
    }
    return $List;
  }
  function Table2Sheet($Table){
    if( sizeof($Table) == 0 ){
      return array();
    }
    $FirstRow = $Table[0];
    $keys = array_keys($FirstRow);

    $Sheet = [$keys];
    foreach($Table as $Row){
      $Sheet[] = array_values($Row);
    }

    return $Sheet;
  }
  function Sheet2Table($Sheet){
    $Table = [];
    $keys = array_shift($Sheet);
    foreach($Sheet as $row){
      $newTableRow = array();
      foreach($keys as $idx => $key ){
        $newTableRow[$key] = $row[$idx];
      }
      $Table[] = $newTableRow;
    }
    return $Table;
  }

  function UTF8(&$array){
    foreach($array as $key => $value){
      $array[$key] = utf8_decode($value);
    }
  }
  function DisplayTable($Sheet,$with_headers=true,$type="Table"){
    if($type=="Table"){
      $Sheet = Table2Sheet($Sheet);
    }

    $keys = array_shift($Sheet);
    $HTML = "<table>";

      $HTML .= "<thead><tr>";
      if($with_headers){foreach($keys as $key){
        $HTML .= "<th>$key</th>";
      }}
      $HTML .= "</tr></thead>";

      $HTML .= "<tbody>";
      foreach($Sheet as $row){
        $HTML .= "<tr>";
        foreach($row as $val){
          $HTML .= "<td>$val</td>";
        }
        $HTML .= "</tr>";
      }
      $HTML .= "</tbody>";

    $HTML .= "</table>";
    return $HTML;
  }

  function Scalar2Array($scalar){
    if( is_array($scalar) ){
      return $scalar;
    }else{
      return [$scalar];
    }
  }

  function jheader(){
    header('Content-Type: application/json; charset=utf-8');
    $r["status"] = false;
    $r["message"] = "";
    $r["data"] = null;
    return $r;
  }
  function recho($Obj){
    if( has_key("message", $Obj) ){
      if( $Obj["message"] != ""){
        $Obj["message"] = trn($Obj["message"]);
      }
    }

    jecho($Obj);
    exit;
  }
  function jecho($Obj){
    echo json_encode( $Obj );
  }
  function isJSON($string){
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
  }

  function trn($message){
    return $message;
  }
