<?php

$Router = new Router();

class Router{
  public $path;
  public $path_list;

  public $method;
  public $params;

  public function __construct(){
    if( array_key_exists("PATH_INFO", $_SERVER) ){
      $this->setPath( $_SERVER["PATH_INFO"] );
    }else{
      $this->setPath( "/" );
    }

    $this->method = $_SERVER["REQUEST_METHOD"];
    parse_str($_SERVER["QUERY_STRING"], $this->params);
  }

  public function setPath($PATH_INFO){
    $this->path = $PATH_INFO;
    $this->path_list = Router::parsePath( $this->path );
  }

  public static function parsePath( $path ){
    $path_list = explode("/", $path);
    array_shift($path_list);
    if( $path_list[0]=="" ){
      $path_list = [];
    }

    foreach($path_list as $idx => $val){
      $path_list[$idx] = strtolower(trim($val));
    }
    return $path_list;
  }

  public function GET( $path, $callbackFn){
    return $this->HANDLER("GET", $path, $callbackFn );
  }
  public function POST( $path, $callbackFn){
    return $this->HANDLER("POST", $path, $callbackFn );
  }
  public function PUT( $path, $callbackFn){
    return $this->HANDLER("PUT", $path, $callbackFn );
  }
  public function DELETE( $path, $callbackFn){
    return $this->HANDLER("DELETE", $path, $callbackFn );
  }
  public function PATCH( $path, $callbackFn){
    return $this->HANDLER("PATCH", $path, $callbackFn );
  }
  public function UPDATE( $path, $callbackFn){
    return $this->HANDLER("UPDATE", $path, $callbackFn );
  }

  public function REQUEST( $path, $callbackFn){
    return $this->HANDLER("ALL", $path, $callbackFn );
  }
  public function HANDLER($method="GET", $path, $callbackFn ){
    if( ! $this->isMethod($method) ){ return; }

    $listener_path_list = Router::parsePath($path);

    foreach( $listener_path_list as $listen_idx => $listener_path ){

      if( ! empty( $this->path_list[$listen_idx])  ){
        $requested_path = $this->path_list[$listen_idx];
      }else{
        if( $listener_path == "*" ){
          break;
        }else{
          return;
        }
      }

      if( $requested_path != $listener_path){
        if( $listener_path == "*" ){
          break;
        }else{
          return;
        }
      }else{
        $is_last_listener = ($listen_idx+1) == sizeof($listener_path_list);
        if( $is_last_listener ){
          if( sizeof($listener_path_list) == sizeof($this->path_list) ){
            break;
          }else{
            return;
          }
        }
      }
    }

    $is_home_path = (sizeof($listener_path_list)==0);
    $home_requested = (sizeof($this->path_list)==0);

    if( $is_home_path ){
      if( ! $home_requested ){
        return;
      }
    }

    return $this->ProcessCallback($callbackFn);
  }

  public function ProcessCallback($callbackFn){
    $ArgsTable = Router::getArguments($callbackFn);
    $argValues = [];
    foreach($ArgsTable as $Arg){
      if( array_key_exists( $Arg["name"], $_REQUEST) ){
        $argValues[] = $_REQUEST[ $Arg["name"] ];
      }else{
        $argValues[] = $Arg["default"];}
    }
    return $callbackFn( ...$argValues );
  }

  public function isMethod($method){
    if( $method=="ALL" ){
      return true;
    }else{
      return ($this->method == $method);
    }
  }

  public static function getArguments($Funcion) {
    $f = new ReflectionFunction($Funcion);
    $result = [];
    foreach( $f->getParameters() as $param) {
      $default = null;
      if( $param->isDefaultValueAvailable() ){
        $default = $param->getDefaultValue();
      }
      $result[] = [
        "name" => $param->name,
        "default" => $default
      ];
    }
    return $result;
  }

}
