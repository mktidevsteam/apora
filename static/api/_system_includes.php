<?php
  ini_set('default_charset', 'utf-8');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: *');

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  include( module('Utils') );
  include( module('Curl') );
  // include( module('Filer') );
  include( module('Router') );
  include( module('Templater') );

  include( module('LogError') );
  $Log = new LogError();

  include( module('Emailer') );
  $Emailer = new Emailer("recepcion@clinicaapora.mx", "Apora Web");

  include( module('SQLite') );
  $SQL = new SQL();
  $SQL->Connect2MySQL($db_name="aporamkt_leads", $db_user="aporamkt_leads", $db_password="N9proJva0L-9", $db_host="clinicaapora.mx");

  // include( module('AppUser') );
  // $User = new AppUser($SQL);

  // include( module('Mailchimper') );
  // $Mailchimp = new Mailchimper("");

  // include( module('Slacker') );
  // $Slack = new Slacker();
  // $Slack->setToken("");

  function module($module_name){
    return ( __DIR__ . "/modules/$module_name.php");
  }
