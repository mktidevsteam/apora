  import Vue from 'vue';

  window.dataLayer = window.dataLayer || [];

  window.dataLayer.event = function( event_name, Params={}){
    Params["event"] = event_name;
    window.dataLayer.push( Params );
  }

  window.dataLayer.install = function (Vue) {
    Vue.prototype.$dataLayer = window.dataLayer;
  }

  Vue.use( window.dataLayer );
