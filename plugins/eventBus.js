import Vue from 'vue';

// EXAMPLE VUE SCROLL
// <a href="#div-id" v-smooth-scroll="{ duration: 1000, offset: -50 }">Anchor</a>
// <div id="div-id"></div>
import vueSmoothScroll from 'vue-smooth-scroll'



const eventBus = {};
eventBus.install = function (Vue) {

  if( process.env.NODE_ENV == "development"){
    Vue.prototype.api = process.env["backend_dir_dev"];
  }else{
    Vue.prototype.api = process.env["backend_dir_gen"];
  }

  const $bus = new Vue();
  Vue.prototype.$bus = $bus;

  Vue.prototype.$today = function(){
    var now = new Date();

    var year = now.getFullYear();
    var month = (now.getMonth() + 1);
    var day = now.getDate();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    return (year + '-' + month + '-' + day);
  };
  Vue.prototype.$getRoute = function(idx=0){
    return this.$route.name.split("-")[idx];
  };
  Vue.prototype.$slugify = function(str){
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  };

  Vue.prototype.$assoc2query = function($Assoc){
    var query_string_parameters = Object.keys($Assoc).map(function(key) {
      return key + '=' + encodeURIComponent($Assoc[key]);
    }).join('&');
    return query_string_parameters;
  }

  Vue.prototype.$cookies = {
    "getGET": function(key){
      var url_string = window.location.href;
      var url = new URL(url_string);
      var value = url.searchParams.get( key );
      if(value){
        return value;
      }else{
        return undefined;
      }
    },
    "set": function(name, value, days) {
      var expires;
      days = days || 180; //6 month default.
      if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
      }
      else {
        expires = "";
      }
      var cookie_string = name+"="+value+expires+"; path=/"
      document.cookie = cookie_string;
      return cookie_string;
    },
    "get": function(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0) === ' ') {
              c = c.substring(1,c.length);
          }
          if (c.indexOf(nameEQ) === 0) {
              return c.substring(nameEQ.length,c.length);
          }
      }
      return null;
    },
    "unset": function(name) {
      this.setCookie(name,"",-1);
    }
  };
};

Vue.use(eventBus);
Vue.use(vueSmoothScroll)
